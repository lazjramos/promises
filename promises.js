var fs = require('fs');

fs.readFile('./texto.txt', function(err, data){
    fs.writeFile('./salida.txt', 'La logintud del texto en el archivo leido es de: '+data.length, function(err){
        console.log(data.length);
    });
});

// // Modularizando el codigo
// function read(fileName){
//     return new Promise(function(resolve, reject){
//         fs.readFile(fileName, function(err, data){
//             if(err){
//                 return reject("No se pudo leer el fichero");
//             }
//             resolve(data);
//         })
//     });
// }

// function write(fileName, content){
//     return new Promise(function(resolve,reject){
//         fs.writeFile(fileName,'La logintud del texto en el archivo leido es de: '+content.length,function(err){
//             if(err){
//                 return reject('No se pudo escribir el fichero')
//             }
//             console.log(content);
//             resolve();
//         })
//     });
// }

// // Permite ejecutar una funcion cuando el array de promesas(lecturas de archivo) se ha realizado
// Promise.all([
//     read('./texto.txt'),
//     read('./salida.txt'),
//     read('./promises.js'),
// ]).then(responses => console.log('Ejecutadas todas las promesas, total de ficheros leidos: '+ responses.length))

// // Permite ejecutar una funcion cuando una de las promesas(lecturas de archivo) ha finalizado... o sea la primera
// Promise.race([
//     read('./texto.txt'),
//     read('./salida.txt'),
//     read('./promises.js'),
// ]).then(response => console.log('Se ha leido el primer fichero con el texto: '+response))

read('./texto.txt')
.then(data => write('./salida.txt',data.length))
.catch(err => console.log('Ocurrio un error: '+ err));